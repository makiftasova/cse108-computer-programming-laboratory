/* ************************************************************************* */
/*  FILENAME: LW06_111044016.c                                               */
/*  Created on 06.04.2012 by Mehmet Akif TASOVA                              */
/*  ------------------------------------------------------------------------ */
/* LW06 - 06.04.2012 - CSE108                                                */
/*                                                                           */
/*  -----------------------------------------------------------------------  */
/*  References                                                               */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */

/* ************************************************************************* */
/*                               INCLUDES                                    */
/* ************************************************************************* */
#include <stdio.h>
#include <math.h>

/* ************************************************************************* */
/*                               DEFINITIONS                                 */
/* ************************************************************************* */
typedef enum {
linear, quadratic, cubic
} function_t; /* a type which includes function types */

/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES                               */
/* ************************************************************************* */

/* Calculate area under the curve */
double areaUnderTheCurve(int a, int b, int n, double fun(double x));

/* Linear Function */
double f(double x);

/* Quadratic Function */
double g(double x);

/* Cubic Function */
double h(double x);

/*select function */
int select(function_t choice);


int main(void)
{
    
    function_t choice; /* variable for storing choice */
    
    char selection; /* variable for getting users choice without errors */
    
    /*
    *remains of part 1 *
    int x, a, b, n; * various variables *
    
    double area; * variable for stroing area */
    
    int isDone=1; /*choice loop breaker */
    
    
    /* begin selection part */
    do{
        printf("[1] Linear Function\n[2] Quadratic Function\n");
        printf("[3] Cubic Function\nEnter your choice: ");
        scanf("%c", &selection);
        
        switch(selection)
        {
            case '1':
                choice = linear;
                isDone = 0;
                break;
        
            case '2':
                choice = quadratic;
                isDone = 0;
                break;
            
            case'3':
                choice = cubic;
                isDone = 0;
                break;
            
            default:
                printf("You entered an invalid choice, plaese try again\n");
                break;
        };
    }while(isDone);
    /* end selection part */
    
    select(choice); /* call select function */
    
    
    /*
    *Reamins of PART 1 *    
    
    printf("Enter beginnig of the interval (a): ");
    scanf("%d", &a);
    
    printf("Enter end of the interval (b): ");
    scanf("%d", &b);
    
    printf("Enter the n value: ");
    scanf("%d", &n);
    
    
    area = areaUnderTheCurve(a, b, n, f);
    
    printf("area: %f\n", area);*/
    
    return 0;
}

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                             */
/* ************************************************************************* */
/* Calculate area under the curve */
double areaUnderTheCurve(int a, int b, int n, double fun(double x))
{
    double h;
    
    double t1, t2;
    
    double stack; /* stack for sums */
    
    int i; /* counter for for-loop */
    
    h = ((b - a) / (double) n); /* calculte h */
    
    t1 = 0.0;
    
    for (i = 1; i < n; i+=1) /* calcualte particular increasments */
    {
        stack = fun(a + (h * i)) * 2;
        
        t1 += stack;
    }
    
    t2 = (fun(a) + fun(b) + (t1)) * (h / 2); /* calcualate area value */
    
    return t2;
}

/* Linear Function */
double f(double x)
{
    return (x + 5);
}

/* Quadratic Function */
double g(double x) 
{
    return ((2 * pow(x, 2)) - (3 * x) + 1);
}

/* Cubic Function */
double h(double x)
{
    return (pow(x, 3));
}


/* select function */

int select(function_t choice)
{
    
    
    int a, b, n;
    
    double area;
    
    /* get the beginning value of intreval (a) */
    printf("Enter beginnig of the interval (a): ");
    scanf("%d", &a);
    
    /* get the ending value of interval (b) */
    printf("Enter end of the interval (b): ");
    scanf("%d", &b);
    
    /* get the n value */
    do{
        printf("Enter the n value: ");
        scanf("%d", &n);
        /* if errrs could be happen */
        if(n == 0)
        {
            printf("If you enter n = 0, it will cause an Zero Division Error \n");
        }
        else if(n < 0)
        {
            printf("n can't ne lower than zero, please enter again\n");
        };
    }while(n <= 0);
    
    
    
    switch(choice)
    {
        case linear: /* calculate by linear function */
            area = areaUnderTheCurve(a, b, n, f);
            break;
        
        case quadratic: /*calculate by quadratic function */
            area = areaUnderTheCurve(a, b, n, g);
            break;
        
        case cubic: /* calculate by cubic function */
            area = areaUnderTheCurve(a, b, n, h); 
            break;
        
        default:
            printf("İnvalid choice\n");
    };
    
    printf("area: %f\n", area); /* print area value to screen */
    
    return 1; /* return 1 if execution successful */
}

/* ************************************************************************* */
/*                         END OF LW06_111044016.c                           */
/* ************************************************************************* */
