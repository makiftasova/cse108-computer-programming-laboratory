/* ************************************************************************* */
/*  FILENAME: LW02_111044016_part1.c              							 */
/*  Created on 02.02.2012 by Mehmet Akif TASOVA     		           		 */
/*	Student Number: 111044016												 */
/*  ------------------------------------------------------------------------ */
/* This program calculates factorial of given number by using function below */
/* n!= (n ^ n) * (e ^ -n) * sqrt((2 * pi * n) + (1 / 3))		       		 */
/* ************************************************************************* */

/* ************************************************************************* */
/*               				 INCLUDES    		                         */
/* ************************************************************************* */
#include <stdio.h>
#include <math.h>

/* ************************************************************************* */
/*                     		 	 DEFINITIONS  						         */
/* ************************************************************************* */
#define PI	3.14159265 /* Mathematical constant pi */

int main(void)
{
	
	double in1, in2, in3, in4, in5; /* variables to store input values */
	double out1_1, out1_2, out1_3; /* variables to store 1st input value */
	double out2_1, out2_2, out2_3; /* variables to store 2nd input value */
	double out3_1, out3_2, out3_3; /* variables to store 3rd input value */
	double out4_1, out4_2, out4_3; /* variables to store 4th input value */
	double out5_1, out5_2, out5_3; /* variables to store 5th input value */
	
	/*variables named out_*_3 are stores final results of calculations */
	
	FILE * inputFile, * outputFile; /* variables for file operations */
	
	printf("\nOpening input.txt File...\n");
	
	inputFile = fopen("input.txt","r"); /* open input.txt as inputFile */
	
	printf("Input File Successfuly Opened!...\n");
	printf("Importing Data...\n");
	
	/* Read valus from inputFile */
	fscanf(inputFile, "%lf %lf %lf %lf %lf", &in1, &in2, &in3, &in4, &in5);
	
	fclose(inputFile); /* close the input.txt */
	
	printf("Importing Input File Successfuly Done!...\n");
	
	outputFile = fopen("output.txt", "w");
	printf("Output File Creation Successfuly Done!...\n");
	
	printf("Doing Calculations and Writing Results Into output.txt File...\n");
	
	/* Start Calculating for input1 and write it into file */
	out1_1 = pow(in1, in1) * exp(-in1);
	out1_2 = sqrt((2 * PI * in1) + (1.0 / 3.0));
	out1_3 = out1_1 * out1_2;
	fprintf(outputFile, "%4d! equals approximately %20f\n", (int) in1, out1_3);
	
	/* Start Calculating for input2 and write it into file */
	out2_1 = pow(in2, in2) * exp(-in2);
	out2_2 = sqrt((2 * PI * in2) + (1.0 / 3.0));
	out2_3 = out2_1 * out2_2;
	fprintf(outputFile, "%4d! equals approximately %20f\n", (int) in2, out2_3);
	
	/* Start Calculating for input1 and write it into file */
	out3_1 = pow(in3, in3) * exp(-in3);
	out3_2 = sqrt((2 * PI * in3) + (1.0 / 3.0));
	out3_3 = out3_1 * out3_2;
	fprintf(outputFile, "%4d! equals approximately %20f\n", (int) in3, out3_3);
	
	/* Start Calculating for input1 and write it into file */
	out4_1 = pow(in4, in4) * exp(-in4);
	out4_2 = sqrt((2 * PI * in4) + (1.0 / 3.0));
	out4_3 = out4_1 * out4_2;
	fprintf(outputFile, "%4d! equals approximately %20f\n", (int) in4, out4_3);
	
	/* Start Calculating for input1 and write it into file */
	out5_1 = pow(in5, in5) * exp(-in5);
	out5_2 = sqrt((2 * PI * in5) + (1.0 / 3.0));
	out5_3 = out5_1 * out5_2;
	fprintf(outputFile, "%4d! equals approximately %20f\n", (int) in5, out5_3);
	
	/* Calculations done */
	printf("Calcultions Successfuly Done!...\n");
	
	fclose(outputFile); /* Close the outputFile after results have written */
	
	printf("Calculations Successfuly Done!...\n\n");
	
	return 0;
}
/* ************************************************************************* */
/*                         END OF LW02_111044016_part1.c                 	 */
/* ************************************************************************* */
