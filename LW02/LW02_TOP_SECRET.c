/* ************************************************************************* */
/*  FILENAME: LW02_111044016_part1.c              		              		 */
/*  Created on 02.02.2012 by Mehmet Akif TASOVA     		           		 */
/*	sTUDENT nUMBER: 111044016												 */
/*  ------------------------------------------------------------------------ */
/* this program calculates factorial of given number by using function below */
/* n!= (n ^ n) * (e ^ -n) * sqrt((2 * pi * n) + (1 / 3))		       		 */
/* ************************************************************************* */

/* ************************************************************************* */
/*               				 INCLUDES    		                         */
/* ************************************************************************* */
#include <stdio.h>
#include <math.h>

/* ************************************************************************* */
/*                     		 	 DEFINITIONS  						         */
/* ************************************************************************* */
#define PI	3.14159265	/* Mathematical constant pi */

/*#define CALCULATE_EXP_PART(number)	(pow(n, n) * pow(exp(-n)))
#define CALCULATE_SQRT_PART(number)	(sqrt((2 * PI * n) + (1 / 3)))*/

int main(void)
{
	
	int in1, in2, in3, in4, in5; /* variables to store input values */
	double out1, out2, out3, out4, out5; /* variables to store output values */
	
	FILE * inputFile, * outputFile; /* variables for file operations */
	
	inputFile = fopen("input.txt","r"); /* open input.txt as inputFile */
	
	/* Read valus from inputFile */
	fscanf(inputFile, "%d%d%d%d%d", &in1, &in2, &in3, &in4, &in5);
	
	fclose(inputFile); /* close the input.txt */
	
	
	/* Start Calculating for input1 and write it into file */
	out1 = CALCULATE_EXP_PART(in1) * CALCULATE_SQRT_PART(in1);
	
	return 0;
}
/* ************************************************************************* */
/*                         END OF LW02_111044016_part1.c                 	 */
/* ************************************************************************* */
