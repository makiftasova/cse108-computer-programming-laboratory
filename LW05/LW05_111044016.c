/* ************************************************************************* */
/*  FILENAME: LW05_111044016.c                                               */
/*  Created on 30.03.2012 by Mehmet Akif TASOVA                              */
/*  ------------------------------------------------------------------------ */
/* [INFORMATION ABOUT WHY YOU WROTE THIS CODE]                               */
/*                                                                           */
/*  -----------------------------------------------------------------------  */
/*  References                                                               */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */

/* ************************************************************************* */
/*                               INCLUDES                                    */
/* ************************************************************************* */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* ************************************************************************* */
/*                               DEFINITIONS                                 */
/* ************************************************************************* */
#define MAX_ROLLS 5000
#define MIN_ROLLS 100000
#define MAX_REPRESENT 40

/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES                               */
/* ************************************************************************* */

/* The function which one generates a random integer */
int getRandomInt(int minimum, int maximum);

/* Function for making everything in a function :) */
int rollDice(int numOfTurns, int *one, int *two, int *three, int *four, int *five, int *six);

/* Function for prointing histogram */
int printHistogram(int one, int two, int three, int four, int five, int six, int timesPerStar);

/*function for oasking continue question*/
char askQuestion();

/* function for finding gretest of given numbers */
int findGreatest(int one, int two,int three, int four, int five, int six);


int main(void)
{
    int times=0, dice=0, counter=0, one=0, two=0, three=0, four=0, five=0, six=0;
    int totalPrint=0, printOne=0, printTwo=0, printThree=0, printFour=0;
    int printFive=0, printSix=0, timesPerStar=0, great;
    char choice;
    
    srand(time(NULL));


    do{
        times = getRandomInt(MIN_ROLLS, MAX_ROLLS);
        
        rollDice(times, &one, &two, &three, &four, &five, &six);
        
        great = findGreatest(one, two,three, four, five, six);
        timesPerStar = great / MAX_REPRESENT; /* Calculate the logest star sequence */
        
        totalPrint = printHistogram(one, two, three, four, five, six, timesPerStar);
        printf("%d\n", totalPrint);
        
        choice = askQuestion();

    }
    while(choice != 'n');


    return 0;
}

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                             */
/* ************************************************************************* */
int getRandomInt(int minimum, int maximum)
{
    int temp=0;
    
    if (minimum < maximum)
    {
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    else if(minimum > maximum)
    {
        temp = maximum;
        maximum = minimum;
        minimum = temp;
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    else
    {
        printf("ERROR: Invalid parameters!!!\n Please check your parameters...\n");
        return -1;
    };
}

int rollDice(int numOfTurns, int *one, int *two, int *three, int *four, int *five, int *six)
{
    int dice, counter=0;
    
    printf("Number of Turns: %d \n", numOfTurns);
    
    for(counter = 0; counter < numOfTurns; ++counter)
    {
        dice = getRandomInt(1,6);
        
        switch(dice)
        {
            case 1:
                *one += 1;
                break;
            case 2:
                *two += 1;
                break;
            case 3:
                *three += 1;
                break;
            case 4:
                *four += 1;
                break;
            case 5:
                *five += 1;
                break;
            case 6:
                *six += 1;
                break;
            default:
                printf("Unrecognized input");
                return -1; /* Return -1 if errors occurred */
                break;
        };
    };
    return 0; /*return 1 if execution succesful */
}

int printHistogram(int one, int two, int three, int four, int five, int six, int timesPerStar)
{
    int numOfOuts, totalOuts=0, counter=0, lineHead=1;
    int printOne, printTwo, printThree, printFour, printFive, printSix;
    
    if ((one >= 0) && (two >= 0) && (three >= 0) && (four >= 0) && (five >= 0) && (six >= 0))
    {
    
        /* print * for one */
        printf("%d: ", lineHead);
        one /= timesPerStar;
        for (counter = 0; counter < one; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        /* print * for two */
        printf("%d: ", lineHead);
        two /= timesPerStar;
        for (counter = 0; counter < two; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        /* print * for three */
        printf("%d: ", lineHead);
        three /= timesPerStar;
        for (counter = 0; counter < three; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        /* print * for four */
        printf("%d: ", lineHead);
        four /= timesPerStar;
        for (counter = 0; counter < four; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        /* print * for five */
        printf("%d: ", lineHead);
        five /= timesPerStar;
        for (counter = 0; counter < five; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        /* print * for six */
        printf("%d: ", lineHead);
        six /= timesPerStar;
        for (counter = 0; counter < six; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        printf("Each * represents %d rolls\n", timesPerStar);
        
        return totalOuts; /* returns number of printed chars */
    }
    else
    {
        printf("You cant print something lower than zero times");
        return -1; /*returns -1 if one of inputs lower than 0 */
    };
}

char askQuestion()
{
    char answer;
    
    do{
        printf("Dou you want to continue [Y/N]: ");
        scanf("%c", &answer);
        
        switch(answer)
        {
            case 'y':
            case 'Y':
                return 'y';
                break;
            
            case 'n':
            case 'N':
                return 'n';
                break;
            default:
                printf("Unrecognized Choice, please enter again\n\n");
                break;
        };
    }while((answer != 'n') || (answer != 'N') || (answer != 'y') || (answer != 'Y'));
}

int findGreatest(int one, int two,int three, int four, int five, int six)
{
    if(one > two)
    {
        if(one > three)
        {
            if(one > four)
                {
                    if(one > five)
                    {
                        if(one > six)
                        {
                            return one;
                        }
                        else
                        {
                            return six;
                        };
                    }
                    else
                    {
                        if(five > six)
                        {
                            return five;
                        }
                        else
                        {
                            return six;
                        };
                    };
                }
                else
                {
                    if(four > five)
                    {
                        if(four > six)
                        {
                            return four;
                        }
                        else
                        {
                            return six;
                        };
                    };
                };
            
        }
        else
        {
            if(three > four)
            {
                if(three > five)
                {
                    if(three > six)
                    {
                        return three;
                    };
                }
            }
        };
    }
    else
    {
        if(two > three)
        {
            if(two > four)
            {
                if(two > five)
                {
                    if(two > six)
                    {
                        return two;
                    };
                };
            };
        };
    };
}

/* ************************************************************************* */
/*                         END OF LW05_111044016.c                           */
/* ************************************************************************* */
