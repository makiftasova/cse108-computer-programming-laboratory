/* ************************************************************************* */
/*  FILENAME: LW05_111044016.c                                               */
/*  Created on 30.03.2012 by Mehmet Akif TASOVA                              */
/*  ------------------------------------------------------------------------ */
/* [INFORMATION ABOUT WHY YOU WROTE THIS CODE]                               */
/*                                                                           */
/*  -----------------------------------------------------------------------  */
/*  References                                                               */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */

/* ************************************************************************* */
/*                               INCLUDES                                    */
/* ************************************************************************* */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* ************************************************************************* */
/*                               DEFINITIONS                                 */
/* ************************************************************************* */
#define MAX_ROLLS 100
#define MIN_ROLLS 10

/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES                               */
/* ************************************************************************* */

/* The function which one generates a random integer */
int getRandomInt(int minimum, int maximum);

/* Function for making everything in a function :) */
int rollDice(int numOfTurns, int *one, int *two, int *three, int *four, int *five, int *six);

/* Function for prointing histogram */
int printHistogram(int one, int two, int three, int four, int five, int six);



int main(void)
{
    int times=0, dice=0, counter=0, one=0, two=0, three=0, four=0, five=0, six=0;
    int totalPrint=0, printOne=0, printTwo=0, printThree=0, printFour=0;
    int printFive=0, printSix=0;
    
    srand(time(NULL));
    
    do
    {
        printf("Enter how many times to roll dice: ");
        scanf("%d", &times);
    }while((times < MIN_ROLLS) || (times > MAX_ROLLS));
    
    for(counter = 0; counter < times; ++counter)
    {
        dice = getRandomInt(1,6);
        
        switch(dice)
        {
            case 1:
                one += 1;
                break;
            case 2:
                two += 1;
                break;
            case 3:
                three += 1;
                break;
            case 4:
                four += 1;
                break;
            case 5:
                five += 1;
                break;
            case 6:
                six += 1;
                break;
            default:
                printf("Unrecognized input");
                break;
        };
    };
    /*
    rollDice(times, &one, &two, &three, &four, &five, &six);
    
    totalPrint = printHistogram(one, two, three, four, five, six);
    printf("%d\n", totalPrint);
*/

    return 0;
}

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                             */
/* ************************************************************************* */
int getRandomInt(int minimum, int maximum)
{
    int temp=0;
    
    if (minimum < maximum)
    {
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    else if(minimum > maximum)
    {
        temp = maximum;
        maximum = minimum;
        minimum = temp;
        return (rand() % ((maximum - minimum + 1)) + minimum);
    }
    else
    {
        printf("ERROR: Invalid parameters!!!\n Please check your parameters...\n");
        return -1;
    };
}

int rollDice(int numOfTurns, int *one, int *two, int *three, int *four, int *five, int *six)
{
    int dice, counter=0;
    
    for(counter = 0; counter < numOfTurns; ++counter)
    {
        dice = getRandomInt(1,6);
        
        switch(dice)
        {
            case 1:
                *one += 1;
                break;
            case 2:
                *two += 1;
                break;
            case 3:
                *three += 1;
                break;
            case 4:
                *four += 1;
                break;
            case 5:
                *five += 1;
                break;
            case 6:
                *six += 1;
                break;
            default:
                printf("Unrecognized input");
                return -1; /* Return -1 if errors occurred */
                break;
        };
    };
    return 0; /*return 1 if execution succesful */
}

int printHistogram(int one, int two, int three, int four, int five, int six)
{
    int numOfOuts, totalOuts=0, counter=0, lineHead=1;
    if ((one >= 0) && (two >= 0) && (three >= 0) && (four >= 0) && (five >= 0) && (six >= 0))
    {
    
        printf("%d: ", lineHead);
        for (counter = 0; counter < one; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        printf("%d: ", lineHead);
        for (counter = 0; counter < two; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        printf("%d: ", lineHead);
        for (counter = 0; counter < three; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        printf("%d: ", lineHead);
        for (counter = 0; counter < four; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        printf("%d: ", lineHead);
        for (counter = 0; counter < five; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        lineHead += 1;
        
        printf("%d: ", lineHead);
        for (counter = 0; counter < six; ++counter)
        {
            numOfOuts = printf("*");
            totalOuts += numOfOuts;
        };
        printf("\n");
        
        return totalOuts;
    }
    else
    {
        printf("You cant print something lower than zero times");
        return -1;
    };
}

/* ************************************************************************* */
/*                         END OF LW05_111044016.c                           */
/* ************************************************************************* */
