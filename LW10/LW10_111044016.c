/* *********************************************************************** */
/*  FILENAME: LW10_111044016.c                                             */
/*  Created on 11.05.2012 by Mehmet Akif TASOVA                            */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <math.h>

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

/* Bool Values */
#define TRUE 1
#define FALSE 0

#define MAX_LEN 100

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* element checker */
int isElement(int set[], int element, int size);

/* disjoint checker */
int isDisjoint(int set1[], int set2[], int size1, int size2);

/* number of digit counter */
int getNumOfDigits(int number);

/* Recursive int reverser function */
int intRecReverser(int number, int numOfDigits);

/* integer reverser function */
int intReverser(int number);

/*check is set */
int isSet(int set[], int size);

/* One Function To Rule Them All */
int main(void)
{
    int set1[]={6,7,8,10,9}, set2[]={1,2,3,3,5};
    int element = 6, size1=5, size2=5;
    int ele1, ele2;
    
    int number = 12345, len;
    
    ele1 = isElement(set1, element, size1);
    ele2 = isElement(set2, element, size2);
    
    printf("Is %d is element of set1: %d\n", element, ele1);
    printf("Is %d is element of set2: %d\n", element, ele2);
    
    printf("Is sets are disjoint: %d\n", isDisjoint(set1,set2,size1,size2));
    printf("number: %d reverted: %d\n", number, intReverser(number));
    
    printf("is set1 is set : %d\n", isSet(set1, size1));
    printf("is set2 is set : %d\n", isSet(set2, size2));
    
    return 0;
}
/*End of Main Function*/

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */

/* element checker */
int isElement(int set[], int element, int size)
{
    int result=FALSE;
    
    if(size >= 1){
        if(set[0] == element)
            result = TRUE;
        else if(set[0] != element)
            result = isElement(&set[1], element, (size-1));
    };
    
    return result;
}

/* disjoint checker */
int isDisjoint(int set1[], int set2[], int size1, int size2)
{
    int result=FALSE, result1, result2;
    
    if((size1 == 0) || (size2 == 0))
        result = FALSE;
    
    if(size1 >= 1){
        result1 = !(isElement(set1, set2[0], size1));
        if(result1 != 0)
            result1 += !(isElement(&set1[1], set2[0], size1));
    };
    
    return result1;
}

/* number of digit counter */
int getNumOfDigits(int number)
{
    int length=0;
    
    if(number > 0){
        if(number < 10){
            length = 1;
        } 
        else {
            length = 1 + getNumOfDigits(number/10);
        };
    }
    else {
        length = 0;
    };
    
    return length;
}

/* Recursive int reverser function */
int intRecReverser(int number, int numOfDigits)
{
    int revNum=0, ones;
    if(numOfDigits > 0){
        if(number < 10){
            revNum = (number * pow(10, (numOfDigits-1)));
        }
        else{
            ones = (number % 10);
            revNum = (ones * pow(10, (numOfDigits-1))) + intRecReverser(number/10, (numOfDigits-1));
        };
        
    };
    return revNum;
}

/* integer reverser function */
int intReverser(int number)
{
    int numOfDigits, revNum;
    
    numOfDigits = getNumOfDigits(number);
    revNum = intRecReverser(number, numOfDigits);
    
    return revNum;
}

/*check is set */
int isSet(int set[], int size)
{
    int result;
    
    if(size == 0)
        result = TRUE;
    if(size > 0)
    {
        result = !(isElement(&set[1], set[0], size));
        
        if(result != 0)
            result = isSet(&set[1], (size - 1));
    };
    
    return result;
}

/* *********************************************************************** */
/*                         END OF LW10_111044016.c                         */
/* *********************************************************************** */
