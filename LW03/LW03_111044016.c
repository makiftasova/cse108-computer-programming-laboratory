/* ************************************************************************* */
/*  FILENAME: LW03_111044016.c                                      		 */
/*  Created on 09.03.2102 by Mehmet Akif TASOVA                		 		 */
/*  ------------------------------------------------------------------------ */
/* This program calculates the avarage grade of student 					 */
/*		and prints it to screen up to 5 exams        				  		 */
/*                            				                                 */
/* ************************************************************************* */

/* ************************************************************************* */
/*               				 INCLUDES    		                         */
/* ************************************************************************* */
#include <stdio.h>

/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES	                             */
/* ************************************************************************* */
char gradeToLetter (double grade); 
	/* This function converts grade to letter code */

double calculateAverage(double totalGrade, int numberOfGrades);
	/* This function calculates average of grades */
	


int main(void)
{
    double grade1, grade2, grade3, grade4, grade5;
    	/* Variables to store grades of student */
    
    double averageGrade=0, /* Variable to store average grade */
    	   totalGrade=0; /* Variable to store total amount of grades */
    	   
    int totalGradeQty=0; /* counter to store total number of grades */
    	
    char letterCode; /* Varible to store letter code of grades */
    
    printf("Enter Total Number of Exams: ");
    scanf("%d", &totalGradeQty); 
       /* Get maximum number of exams and assing it to variable totalGradeQty */
    
    if (totalGradeQty == 0)
    {
    	printf("You Said \"I did NO exams\" \n");
    	printf("Are you sure about you are a thinking creature ?\n");
    	return 0;
    }
    if(totalGradeQty > 5)
    {
    	printf("Unfortunately, this program dowsn't support more than 5 exams\n");
    	printf("Maybe it will be updated later for this issue...\n");
    	return 0;
    };
    
       
    /* This part gets the grades for each exam. If exam grades			 */
    /*		ouf of range 0-100, cut off taking grades from user and exit */
    	/* If only one exam presents then call this part */
    if(totalGradeQty == 1)
    {
    	printf("Enter grade of 1st exam: ");
    	scanf("%lf", &grade1);
    	if ((grade1 < 0) || (grade1 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	totalGrade = grade1;
    }
    
    	/* If two exams presents then call this part */
    if(totalGradeQty == 2)
    {
    	printf("Enter grade of 1st exam: ");
    	scanf("%lf", &grade1);
    	if ((grade1 < 0) || (grade1 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 2nd exam: ");
    	scanf("%lf", &grade2);
    	if ((grade2 < 0) || (grade2 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	totalGrade = (grade1 + grade2);
    }
    
    /* If three exams presents then call this part */
    if(totalGradeQty == 3)
    {
    	printf("Enter grade of 1st exam: ");
    	scanf("%lf", &grade1);
    	if ((grade1 < 0) || (grade1 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 2nd exam: ");
    	scanf("%lf", &grade2);
    	if ((grade2 < 0) || (grade2 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 3rd exam: ");
    	scanf("%lf", &grade3);
    	if ((grade3 < 0) || (grade3 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	totalGrade = (grade1 + grade2 + grade3);
    }
    
    /* If four exams presents then call this part */
    if(totalGradeQty == 4)
    {
    	printf("Enter grade of 1st exam: ");
    	scanf("%lf", &grade1);
    	if ((grade1 < 0) || (grade1 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 2nd exam: ");
    	scanf("%lf", &grade2);
    	if ((grade2 < 0) || (grade2 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 3rd exam: ");
    	scanf("%lf", &grade3);
    	if ((grade3 < 0) || (grade3 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 4th exam: ");
    	scanf("%lf", &grade4);
    	if ((grade4 < 0) || (grade4 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	totalGrade = (grade1 + grade2 + grade3 + grade4);
    }
    
    /* If five exams presents then call this part */
    if(totalGradeQty == 5)
    {
    	printf("Enter grade of 1st exam: ");
    	scanf("%lf", &grade1);
    	if ((grade1 < 0) || (grade1 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 2nd exam: ");
    	scanf("%lf", &grade2);
    	if ((grade2 < 0) || (grade2 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 3rd exam: ");
    	scanf("%lf", &grade3);
    	if ((grade3 < 0) || (grade3 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 4th exam: ");
    	scanf("%lf", &grade4);
    	if ((grade4 < 0) || (grade4 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	printf("Enter grade of 5th exam: ");
    	scanf("%lf", &grade5);
    	if ((grade5 < 0) || (grade5 > 100) )
    	{
    		printf("ERROR: Exam grade out of range!!!\n");
    		return 0;
    	};
    	
    	totalGrade = (grade1 + grade2 + grade3 + grade4 + grade5);
    };
    
    	/* End of get exam grades part */
    
    averageGrade = calculateAverage(totalGrade, totalGradeQty);
    
    letterCode = gradeToLetter(averageGrade);
    
    if (letterCode != '0')
    {
    	printf("This students average of exams %.3f \n", averageGrade);
    	printf("This students letter code: %c \n", letterCode);
    }
    else
    {
    	printf("ERROR:  Average of Exams out of range!!!\n");
    };
    return 0;
}

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                   			 */
/* ************************************************************************* */

char gradeToLetter (double grade)
{
	/* convert grade to letter code if grade in range 0 - 100*/
	if((grade > -(0.1)) && (grade < 100.1)) 
	{
		if (grade > 99.9)
		{
			return 'A';
		}
		if ((grade < 99.9) && (grade > 79.9))
		{
			return 'B';
		}
		if ((grade < 79.9) && (grade > 49.9))
		{
			return 'C';
		}
		if ((grade < 49.9) && (grade > 19.9))
		{
			return 'D';
		}
		if ((grade < 19.9) && (grade > 0.9))
		{
			return 'E';
		}
		if (grade < 0.1)
		{
			return 'F';
		};
	}
	/* if grade out of range 0 - 100 return char 0 */
	else
	{
		return	'0'; /* Function returns char 0 if grade out of range  */
	};
}

double calculateAverage(double totalGrade, int numberOfGrades)
{
	/* This will casue return NULL because of Zero Division Error */
	if(numberOfGrades == 0) 
	{
		printf("ERROR: Zero Divion Error!!!");
		return '\0';
	}
	else
	{
		return (totalGrade / numberOfGrades);
	}
}


/* ************************************************************************* */
/*                         END OF LW03_111044016.c                      	 */
/* ************************************************************************* */
