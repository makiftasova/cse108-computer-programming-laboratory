/*############################################################################*/
/*                      LW00_111044016_PART_1.c                               */
/*                                                                            */
/*               Created on 24.02.2012 by Memet Akif TAŞOVA                   */
/*                                                                            */
/*  Description                                                               */
/*  -----------                                                               */
/*                                                                            */
/*  This Program converts given Celcius degree to Fahrenheit degree           */      
/*                                                                            */
/*############################################################################*/

/**************/
/*  Includes  */
/**************/

#include <stdio.h>

/*****************/
/*  Definitions  */
/*****************/

#define MULTIPLER_CEL_TO_FAH 1.8  /* Convertion Multipler Constant */
#define ADD_CEL_TO_FAH 32          /*  Convertion Increase Constant

/*******************/
/*  main Function  */
/*******************/

int main(void)
{
    double celcius,        /* Variable for celcius value taken from user */
           fahrenheit;   /* Variable for fahrenheit value to print screen */
    
    FILE * inputFile, * outputFile; /* Files */
    
    inputFile = fopen("in.txt", "r");
    outputFile = fopen("out.txt", "w");
    
    /* Read ingredents of inputFile */
    fscanf(inputFile, "%lf", &celcius);
    printf("Tempature is %.3f Celcius degree\n", celcius);
    
    fclose(inputFile);  /* Close inputFile because it is requiered no longer */
    
    /* Converting Calcius to fahrenheit */
    fahrenheit = (celcius * MULTIPLER_CEL_TO_FAH) + ADD_CEL_TO_FAH;
    
    /* Print final value of calculation to screen */
    printf("%.3f Celcius degree equals %.3f Fahrenheit degree\n", celcius, fahrenheit); 
    
    /* Write final value of calculation to out.txt file */ 
    fprintf(outputFile, "%.3f", fahrenheit);   
    
    fclose(outputFile); /* Close outputFile after changed */
    
    return 0;

}

