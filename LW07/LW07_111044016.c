/* ************************************************************************* */
/*  FILENAME: LW07_111044016.c                                               */
/*  Created on 13.04.2012 by Mehmet Akif TASOVA                              */
/*  ------------------------------------------------------------------------ */
/* Implementation of Pythagorean Theorem                                     */
/* Progeam will compute distance between some points and finds the           */
/*   maximum distance                                                        */
/*                                                                           */
/* ************************************************************************* */

/* ************************************************************************* */
/*                               INCLUDES                                    */
/* ************************************************************************* */
#include <stdio.h>
#include <math.h>

/* ************************************************************************* */
/*                               DEFINITIONS                                 */
/* ************************************************************************* */
#define MAX_SIZE_X 15 /* 2D array's first dimension */
#define MAX_SIZE_Y 2 /* 2D array's second dimension */
/* ************************************************************************* */
/*                         FUNCTION PROTOTYPES                               */
/* ************************************************************************* */

/* Read points from txt file */
int readPoints(FILE *inputFile, double points[][MAX_SIZE_Y], int capacityofArray);

/* remains of part 1 */
/*calculate distance between points */
/*double calcualteDistance(double point1[][MAX_SIZE_Y], double point2[][MAX_SIZE_Y]);*/


/* find max distance of points */
double findMaxDistance(double points[][MAX_SIZE_Y], int sizeOfArray);

/* Beginnig of Main Function */
int main(void)
{
    double points[MAX_SIZE_X][MAX_SIZE_Y]; /* array for storing points */
    
    double distance;
    
    int capacityofArray; /* variable for storing array's length */
    
    int numOfPoints; /* variable for storing number of points */
    
    FILE *inputFile; /* input file */
    
    inputFile = fopen("points.txt", "r"); /* open input file */
    
    numOfPoints = readPoints(inputFile, points, MAX_SIZE_X); /*read points */
    
    fclose(inputFile); /* close input file */
    
    /*calcuate distance between second and third point */
    /*distance = calcualteDistance(points, points);
    
    /* remains of part 1 
    /*printf distance to screen 
    /*printf("distance between (%.3f, %.3f) and (%.3f, %.3f) =", points[1][0], points[1][1], points[2][0], points[2][1]);
    /* printf("%.3f\n", distance);
    /* remains of part1 end */
    /*printf("%d point read successfuly\n", numOfPoints);*/
    
    distance = findMaxDistance(points, MAX_SIZE_X);
    
    printf("Max distance = %.3f\n", distance);
    
    return 0;
}
/* End of Main Function */

/* ************************************************************************* */
/*                          FUNCTION DEFINITIONS                             */
/* ************************************************************************* */

/* Read points from txt file */
int readPoints(FILE *inputFile, double points[][MAX_SIZE_Y], int capacityofArray)
{
    int i=0; /* junk value for loops */
    int checkEOF; /* variable for checking EOF status */
    int isEOF=1; /* loop breaker for EOF status */
    int numOfPoints; /* variable for storing number of points */
    int pointsQty; /* number of points which declared in txt file */
    
    fscanf(inputFile, "%d", &pointsQty); /* read Qty of Points from file */
    
    do{
        /* read points data from file */
        checkEOF = fscanf(inputFile ,"%lf %lf", &points[i][0], &points[i][1]);
        if(checkEOF == EOF)
        {
            isEOF = 0;
        };
        i += 1; /* counter for line number */
    }while(i<pointsQty && i<capacityofArray);
    
    numOfPoints = i;
    
    return numOfPoints; /* return total number of read points */
}

/*remains of part 1 */
/*calculate distance between points */

double calcualteDistance(double point1[][MAX_SIZE_Y], double point2[][MAX_SIZE_Y])
{
    double distance = 0; /* variable for stoing distance */
    double x1,x2,y1,y2; /* temp varibles for points */
    double xPart, yPart; /* variables for calculating distance's parts */
    double distanceOld; /* stores old value of distance for comparing */
    int i, j; /* variables for loop of caculating distance between all points */
    
    x1 = point1[1][0];
    x2 = point2[2][0];
    y1 = point1[1][1];
    y2 = point2[2][1];
    
    xPart = pow((x2 - x1), 2);
    yPart = pow((y2 - y1), 2);
    distance = sqrt(xPart + yPart);
    
    return distance;
}


/* find max distance of points */
double findMaxDistance(double points[][MAX_SIZE_Y], int sizeOfArray)
{
    int i, j, loopNo=0;
    double x1, x2, y1, y2; /* temp variables for points */
    double distance = 0; /* variable for stoing distance */
    double xPart, yPart; /* variables for calculating distance's parts */
    double distanceOld; /* stores old distance value for comparing */
    double distanceMax; /* stores maximum distance */
    int maxPoint1=0, maxPoint2=0; /* stores numbers of points */
    int max1, max2; /* return value of maximum distance points */
    
    
    for (i=0; i<sizeOfArray; ++i)
    {
        
        /* get data some more useful */
        x1 = points[i][0];
        y1 = points[i][1];
        
        maxPoint1 += 1;
        
        /*printf("%f, %f \n", x1, y1); /* line for debugging */
        
        for(j=0;j<sizeOfArray; ++j)
        {
            /* get data some more useful */
            x2 = points[j][0]; 
            y2 = points[j][1];
            
            maxPoint2 = (j+1);
            
            /*printf("%f, %f \n", x2, y2); /* line for degugging */
        
            xPart = pow((x2 - x1), 2);
            yPart = pow((y2 - y1), 2);
            distance = sqrt(xPart + yPart);
        
            if(loopNo != 0) /* we don't need comparing for first time of loop */
            {
                if(distance >= distanceOld)
                {
                    distanceMax = distance;
                    distanceOld = distance;
                    max1 = maxPoint1;
                    max2 = maxPoint2;
                    
                }
                else
                {
                    distanceMax = distanceOld;
                };
            };
        
            loopNo += 1; /* counter of loop number */
        };
    };
    
    printf("Maximum distance is between #%d and #%d points\n", max1, max2);
    
    return distanceMax;
}

/* ************************************************************************* */
/*                         END OF LW07_111044016.c                           */
/* ************************************************************************* */
