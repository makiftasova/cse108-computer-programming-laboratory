/* *********************************************************************** */
/*  FILENAME: LW11_111044016_main.c                                        */
/*  Created on 18.05.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Main file of LW11                                                       */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */

#include <stdio.h>

#ifndef SELLER_H
#define SELLER_H
#include "LW11_111044016_seller.h"
#endif


/* Beginning of Main Function */
int main(void)
{
    int numOfItems, i, j;
    char fileName[NAME_LEN];
    FILE *input;

    item_t item[MAX_NUM_OF_ITEMS], readItem[MAX_NUM_OF_ITEMS];
    
    /*                      *****remains of part 1****
    printf("Maximum %d items can add to database.\n", MAX_NUM_OF_ITEMS);
    printf("How many item(s) you want to add database: ");
    do{
        scanf("%d", &numOfItems);
        if((numOfItems < 0) || (numOfItems > 10)){
            printf("You entered an invalid number.\n");
            printf("How many item(s) you want to add database: ");
        };

    }while((numOfItems <= 0) || (numOfItems > MAX_NUM_OF_ITEMS));  
    
                ****end of part 1 remains****                */

    /* read dateset 1 */
    j = printf("\nEnter Dataset 1\n");
    for(i=0;i<j;++i)
        printf("-");
    printf("\n");
    getItem(item, 2);
    
    printf("\nEnter filename to backup>>");
    gets(fileName);
    writeItem(fileName, item, 2);
    input = fopen(fileName, "rb");
    readAnItem(input, readItem, 2);
    fclose(input);
    
    /* read dateset 2 */
    j = printf("\nEnter Dataset 2\n");
    for(i=0;i<j;++i)
        printf("-");
    printf("\n");
    getItem(&item[2], 2);
    
    writeItem(fileName, item, 2);
    input = fopen(fileName, "rb");
    readAnItem(input, readItem, 2);
    fclose(input);
    
    /* read dateset 3 */
    j = printf("\nEnter Dataset 3\n");
    for(i=0;i<j;++i)
        printf("-");
    printf("\n");
    getItem(&item[4], 2);
    
    writeItem(fileName, item, 2);
    input = fopen(fileName, "rb");
    readAnItem(input, readItem, 2);
    fclose(input);
    
    /* ***remains of part 1*** */
    /*
    j = printf("Your Items\n");
    for(i=0;i<j;++i)
        printf("-");
    printf("\n");
    printf("ID  -   Name   -  Price\n");
    for(i=0;i<numOfItems;++i)
        printItem(item[i]);
    */
    /* *** end of remains of part 1 *** */
    /*
    printf("Enter filename to backup>>");
    gets(fileName);
    writeItem(fileName, item, 6);
    
    input = fopen(fileName, "rb");
    readAnItem(input, readItem, 6);
    fclose(input);*/
    
    return 0;
}
/*End of Main Function*/

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */


/* *********************************************************************** */
/*                         END OF LW11_111044016_main.c                    */
/* *********************************************************************** */
