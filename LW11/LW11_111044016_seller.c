/* *********************************************************************** */
/*  FILENAME: LW11_111044016_seller.c                                      */
/*  Created on 18.05.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Function implemention file of LW11                                      */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#ifndef SELLER_H
#define SELLER_H
#include "LW11_111044016_seller.h"
#endif

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */
item_t getItem(item_t *item, int numOfItems)
{
    int id,count, i, printLen;
    double price;
    char name[NAME_LEN], junk;
    

    for(count=0; count<numOfItems; ++count){
        printf("\n");
        printLen = printf("Enter item #%d\n", (count +1 ));
        for(i=0;i<printLen;++i)
            printf("-");
        printf("\n");

        printf("Enter item id>>");
        scanf("%d", &id);
        scanf("%c", &junk);

        printf("Enter item name>>");
        gets(name);

        item->name[NAME_LEN-1] =  NULL_CHAR;
                              
        printf("Enter item price>>");
        scanf("%lf", & price);
        scanf("%c", &junk);

        strcpy(item[count].name, name);
        item[count].id = id;
        item[count].price = price;
        
    };

    return *item;
}
void printItem(item_t item)
{
    printf("%4d - %s - %6.2f \n", item.id, item.name, item.price);

    return;
}

/* Part 2 Functions */
void writeItem(char* filename, item_t *item, int numOfItems)
{
    FILE *output;
    int sizeOfStruct, i;
    
    sizeOfStruct = sizeof(item_t);
    output = fopen(filename, "ab");
    
    fwrite(item, sizeOfStruct, numOfItems, output);
    
    fclose(output);
    return;
}

item_t readAnItem(FILE* file, item_t *item, int numOfItems)
{
    int sizeOfStruct, i, j;
    
    sizeOfStruct = sizeof(item_t);
    
    fread(item, sizeOfStruct, numOfItems, file);
    
    j = printf("Your Items\n");
    for(i=0;i<j;++i)
        printf("-");
    printf("\n");
    printf("ID  -   Name   -  Price\n");
    for(i=0;i<numOfItems;++i)
        printItem(item[i]);
    
    return *item;
}

/* *********************************************************************** */
/*                         END OF [PROJECT NAME].c                         */
/* *********************************************************************** */
