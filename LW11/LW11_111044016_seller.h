/* *********************************************************************** */
/*  FILENAME: LW11_111044016_seller.h                                      */
/*  Created on 18.05.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* Function Header file of LW11                                            */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

#define NAME_LEN 50
#define MAX_NUM_OF_ITEMS 10
#define NULL_CHAR '\0'

typedef struct{
                int id;
                char name[NAME_LEN];
                double price;
                } item_t;


/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */
extern item_t getItem(item_t *item, int numOfItems);
extern void printItem(item_t item);

/* Part 2 Functions */
extern void writeItem(char* filename, item_t *item, int numOfItems);
extern item_t readAnItem(FILE* file, item_t *item, int numOfItems);

/* *********************************************************************** */
/*                         END OF LW11_111044016_seller.h                  */
/* *********************************************************************** */
