/* *********************************************************************** */
/*  FILENAME: LW08_111044016.c                                             */
/*  Created on 20.04.2012 by Mehmet Akif TASOVA                            */
/*  ---------------------------------------------------------------------- */
/* part1 - Checks a string is it palindrome or not                         */
/* part2 - discards given letter from string                               */
/*                                                                         */
/* *********************************************************************** */

/* *********************************************************************** */
/*                             INCLUDES                                    */
/* *********************************************************************** */
#include <stdio.h>
#include <string.h>

/* *********************************************************************** */
/*                               DEFINITIONS                               */
/* *********************************************************************** */

#define STR_LEN 80 /* max length of string */

/* *********************************************************************** */
/*                         FUNCTION PROTOTYPES                             */
/* *********************************************************************** */

/* checks a string is it palindrome or not */
int isPalindrome(char *str); /* from part1 */

/* calculates length of given string */
int getLen(char *str); /* from part1 */

/* find and discard given element of string */
char* deleteChar(char str[], char charToDel);
 
/* Beginning of Main Function */
int main(void)
{
    char string[STR_LEN]; /* store string */
    
    char * deletedString; /* stores manipulated string */
    
    char charToDel; /* store char to del */
    
    printf("Enter a string up to 79 chars: ");
    gets(string); /* get string */
    
    printf("Given String: \n");
    printf("%s\n", string); /* print string for reference */
    
    printf("Enter char to del: ");
    scanf("%c", &charToDel); /* get char to delete from user */
    
    deletedString = deleteChar(string, charToDel); /* delete given char */
    
    printf("Manipulated string: \n");
    
    printf("%s\n",deletedString); /* print string using char pointer */
    
    printf("Last form of first string: \n");
    printf("%s\n", string); /* print last form of first string */
    
    return 0;
    
    /* remains of part 1 */
    /* char string[STR_LEN]; /* store string */
    
     /*int isPal; /* stores status of polindrome */
    /*
    printf("Enter a string up to 79 chars: ");
    gets(string); /* get string */
    
    /*isPal = isPalindrome(string); /* check string */
    
    /*if(isPal == 1) /* print result to screen */
    /*{
        printf("String is palindrome\n");
    }
    else
    {
        printf("String is not palindrome\n");
    };
    
    return 0; */
    
    
}


/*End of Main Function*/

/* *********************************************************************** */
/*                          FUNCTION DEFINITIONS                           */
/* *********************************************************************** */
/* checks a string is it palindrome or not */
int isPalindrome(char *str) /* from part1 */
{
    int length; /* length of string */
    
    int i; /* loop counter */
    
    length = getLen(str); /* get length of string */
    
    for (i=0; i<length; ++i) /* check is palindrome */
    {
        if(str[i] != str[(length-(1+i))])
        {
            return 0; /* return 0 if fails */
        };
    };
    
    return 1; /* return 1 if success */
}

/* calculates length of given string */
int getLen(char *str) /* from part1 */
{
    int len; /* integer for storing length of string */
    
    for(len=0; (str[len] != '\0'); ++len);
    
    return len; /* return length of string */
}

/* find and discard given element of string */
char* deleteChar(char str[], char charToDel)
{
    int length;
    
    int i; /* loop counter */
    
    length = getLen(str); /* get length of given string */
    
    for(i=0;i<length;++i) /* search and del given chars */
    {
        if(str[i] == charToDel)
        {
            strcpy(&str[i], &str[i+1]);
            i -= 1;
        };
    };
    
    return str; /* return first memory adress of given string after manupilation */
}

/* *********************************************************************** */
/*                         END OF LW08_111044016.c                         */
/* *********************************************************************** */
